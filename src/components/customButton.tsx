import React from 'react'
import { StyleSheet, TouchableOpacity, Text, Image } from 'react-native'
import { useFonts, Poppins_600SemiBold } from '@expo-google-fonts/poppins'

type AppButtonVariables = {
  onPress?: any
  title?: string
  backgroundColor?: any
}

const images = {
  'barcodeScanner': require('../../assets/img/error/barcode-scanner.png'),
}

const AppButton = ({ onPress, title, backgroundColor } : AppButtonVariables) => {
  let [fontsLoaded] = useFonts({
    Poppins_600SemiBold,
  });

  if (!fontsLoaded) {
    return null
  }

  return (
    <TouchableOpacity
      onPress={onPress}
      style={[
        styles.appButtonContainer,
        backgroundColor && { backgroundColor }
      ]}
    >
      <Text style={[styles.appButtonText, {fontFamily: 'Poppins_600SemiBold'}]}>
        {title}
      </Text>
      <Image source={images.barcodeScanner} />
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  appButtonContainer: {
    elevation: 8,
    backgroundColor: '#009688',
    borderRadius: 10,
    paddingVertical: 14,
    paddingHorizontal: 30,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  appButtonText: {
    fontSize: 18,
    color: '#FFFFFF',
    alignSelf: 'center',
    marginRight: 10,
  }
})

export default AppButton