import React from 'react'
import { Text, View, SafeAreaView, ScrollView, StyleSheet, Platform } from 'react-native'
import AppButton from '../components/customButton'
import AppClickableImage from '../components/customImage'
import ResultBanner from '../components/resultBanner'
import ResultRecord from '../components/resultRecord'
import { Props } from '../../types'
import { useFonts, Poppins_700Bold } from '@expo-google-fonts/poppins'

const images = {
  'qrError': require('../../assets/img/error/qr-error.png'),
  'leftCaret': require('../../assets/img/verificationresult/left-caret.png'),
}

const VerificationResultPage = ({ route, navigation }: Props) => {
  const data = route.params
  const { validationResult } = data

  let [fontsLoaded] = useFonts({
    Poppins_700Bold,
  });

  if (!fontsLoaded) {
    return null
  }

  return (
    <View style={styles.flexContainer}>
      <View style={styles.backButtonContainer}>
        <AppClickableImage
          source={images.leftCaret}
          onPress={() => navigation.navigate('ScanQR')}
        />
        <Text style={[styles.backButtonText, {fontFamily: 'Poppins_700Bold'}]} onPress={() => navigation.navigate('ScanQR')}> Verification result </Text>
      </View>
      <ScrollView>
        <ResultBanner validationResult={validationResult}/>
        {validationResult.isValid &&
          <ResultRecord data={data} />
        }
      </ScrollView>
      <AppButton
        title='Scan next vaccination record'
        onPress={() => navigation.navigate('ScanQR')}
        backgroundColor='#255DCB'
      />
    </View>
  )
}

const styles = StyleSheet.create({
  flexContainer: {
    flex: 1,
    paddingBottom: 30,
    paddingLeft: 20,
    paddingRight: 20,
    justifyContent: 'space-between',
    backgroundColor: '#F3F6FF',
  },
  backButtonContainer: {
    paddingTop: '15%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  backButtonText: {
    paddingLeft: 10,
    fontSize: 20,
    lineHeight: 30,
    color: '#255DCB',
  },
})

export default VerificationResultPage